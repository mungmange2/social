package com.example.social;

import com.example.social.config.VaultData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


@Slf4j
@SpringBootApplication
public class SocialApplication {

    public static void main(String[] args) {

        ConfigurableApplicationContext context = SpringApplication.run(SocialApplication.class, args);

        // vault 테스트 --
//        VaultData vaultData = context.getBean(VaultData.class);
//        log.info("username:{}", vaultData.getUsername());
//        log.info("password:{}", vaultData.getPassword());
    }
}
